#include <stdio.h>

#include <stdlib.h>

#include <ctype.h>

#include <stdbool.h>

#include <time.h>

#include <strings.h>

#define MAX_WORD_LEN 80

#define ALPHABET 26

struct dice

{
	int die_1;
	int die_2;
	int dice_sum;
};

struct dice roll_dice(void);

bool play_game(void);

int main(void)

{


	int wins = 0;

	int losses = 0;

	char choice[2] = "Y";



	srand((unsigned)time(NULL)); // random number generator //

	while (strcasecmp(choice, "N")) 

{
		
play_game() ? ++wins : ++losses;

		
printf("Play again? (y/n): ");
		
scanf("%1s", choice);
	
}
	
printf("wins: %d losses: %d\n", wins, losses);

	return 0;

}


struct dice roll_dice(void)

{
	struct dice roll;

	// Prints roll dice.. and generates random rolls//

	printf("\nRoll dice...\n");

	roll.die_1 = (rand() % 6) + 1;

	roll.die_2 = (rand() % 6) + 1;

	// Adds the two rolls to find sum then prints what you rolled //

	roll.dice_sum = roll.die_1 + roll.die_2;

	printf("| %d |\t| %d |\n", roll.die_1, roll.die_2);

	printf("You rolled: %d\n", roll.dice_sum);

	return roll;

}


bool play_game(void)

{

	bool win = false;

	bool win_point = false;

	int point = 0; 		
   
	struct dice game;

	game = roll_dice();

	switch(game.dice_sum)
 {
		
case 7: case 11: win = true;
			
break;
		
case 2: case 3: case 12: win = false;
			
break;
		
default: point = game.dice_sum;
	
}

	
if (point) {
		
printf("Your point is %d\n", point);
		
do {
			
game = roll_dice();
			
if (game.dice_sum == 7) 
{
				
win_point = true;
				
win = false;

			
}
			
if (game.dice_sum == point) 
{
				
win_point = true;
				
win = true;
			
}
		
} 
while (win_point == false);
	
}

	printf("You %s\n", win ? "Won!" : "Lost");

	return win;
}